<?php declare(strict_types=1);

namespace ThibaudDauce\LaravelFilters;

use Illuminate\Support\Collection;
use Orchestra\Testbench\TestCase;

class CollectionMacroTest extends TestCase
{
    /** @test */
    function it filters a Collection with Filters()
    {
        $filtered = collect([
            'name' => 'JOHN',
        ])->filters(new Filters([
            'name' => ['strtolower'],
        ]));

        $this->assertInstanceOf(Collection::class, $filtered);
        $this->assertEquals('john', $filtered['name']);
    }
    
    /** @test */
    function it filters a Collection with an array of filters()
    {
        $filtered = collect([
            'name' => 'JOHN',
        ])->filters([
            'name' => ['strtolower'],
        ]);

        $this->assertInstanceOf(Collection::class, $filtered);
        $this->assertEquals('john', $filtered['name']);
    }

    /** @test */
    function it returns the correct instance of sub Collection class()
    {
        $filtered = (new SomeCollection([
            'name' => 'JOHN',
        ]))->filters(new Filters([
            'name' => ['strtolower'],
        ]));

        $this->assertInstanceOf(SomeCollection::class, $filtered);
        $this->assertEquals('john', $filtered['name']);
    }

    protected function getPackageProviders($app)
    {
        return [ServiceProvider::class];
    }
}

class SomeCollection extends Collection
{
    
}