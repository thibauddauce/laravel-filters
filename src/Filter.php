<?php declare(strict_types=1);

namespace ThibaudDauce\LaravelFilters;

use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;

class Filter
{
    public $callable;

    public $arguments;

    public function __construct($callable, $arguments = ['$$'])
    {
        $this->callable = $callable;
        $this->arguments = new Collection($arguments);
    }

    public function __invoke($value)
    {
        $arguments = $this->arguments->map(function($argument) use ($value) {
            return $argument === '$$' ? $value : $argument;
        });

        return call_user_func_array($this->callable, $arguments->toArray());
    }

    public static function parse($filter)
    {
        return self::pipeFilterThrough($filter, [
            new Middlewares\SplitArguments,
            new Middlewares\ResolveFilter,
        ]);
    }

    protected static function pipeFilterThrough($filter, $middlewares)
    {
        return (new Pipeline)
            ->send(new self($filter))
            ->through($middlewares)
            ->via('__invoke')
            ->then(function($filter) {
                return $filter;
            });
    }

    protected function isResolvable($filter)
    {
        return Filters::$container AND is_string($filter) AND Filters::$container->bound($filter);
    }

    protected function resolve($filter)
    {
        return Filters::$container->make($filter);
    }
}