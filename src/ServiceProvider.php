<?php declare(strict_types=1);

namespace ThibaudDauce\LaravelFilters;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider
{
    public function boot()
    {
        Filters::$container = $this->app;

        $this->app->resolving('Illuminate\Foundation\Http\FormRequest', function ($request) {
            FilterRequest::before($request);
        });
        $this->app->afterResolving('Illuminate\Foundation\Http\FormRequest', function ($request) {
            FilterRequest::after($request);
        });

        Collection::macro('filters', function ($filters) {
            if (! $filters instanceof Filters) {
                $filters = new Filters($filters);
            }

            return new self($filters($this));
        });
        Request::macro('filter', function ($filters) {
            if (! $filters instanceof Filters) {
                $filters = new Filters($filters);
            }

            $this->query->replace($filters($this->query->all()));

            if ($this->isJson()) {
                $this->json()->replace($filters($this->json()->all()));
            } else {
                $this->request->replace($filters($this->request->all()));
            }

            return $this;
        });

        $this->publishes([
            __DIR__.'/../config/filters.php' => config_path('filters.php'),
        ], 'filters');
    }
}
