<?php declare(strict_types=1);

namespace ThibaudDauce\LaravelFilters\Middlewares;

use ThibaudDauce\LaravelFilters\Filter;
use ThibaudDauce\LaravelFilters\Filters;

class ResolveFilter
{
    public function __invoke(Filter $filter, $callback)
    {
        if (! $this->isResolvable($filter)) {
            return $callback($filter);
        }

        return $callback(new Filter($this->resolve($filter), $filter->arguments));
    }

    protected function isResolvable($filter)
    {
        return Filters::$container AND is_string($filter->callable) AND Filters::$container->bound($filter->callable);
    }

    protected function resolve($filter)
    {
        return Filters::$container->make($filter->callable);
    }
}