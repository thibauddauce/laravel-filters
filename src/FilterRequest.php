<?php declare(strict_types=1);

namespace ThibaudDauce\LaravelFilters;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\ParameterBag;

class FilterRequest
{
    public static function before(Request $request)
    {
        self::handle($request, 'filters');
    }

    public static function after(Request $request)
    {
        self::handle($request, 'afterFilters');
    }

    protected static function handle(Request $request, $method)
    {
        if (! method_exists($request, $method) OR in_array(get_class($request), config('filters.except', []))) {
            return;
        }

        $filters = new Filters($request->$method());

        self::cleanParameterBag($request->query, $filters);

        if ($request->isJson()) {
            self::cleanParameterBag($request->json(), $filters);
        } else {
            self::cleanParameterBag($request->request, $filters);
        }
    }

    /**
     * Clean the data in the parameter bag.
     *
     * @param  \Symfony\Component\HttpFoundation\ParameterBag  $bag
     * @return void
     */
    protected static function cleanParameterBag(ParameterBag $bag, $filters)
    {
        $bag->replace($filters($bag->all()));
    }
}